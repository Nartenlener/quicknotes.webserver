import mongoose, { Document, Schema } from 'mongoose';
import * as cfg from '../configurations/serverConfiguration'

export interface INote extends Document {
    note: String
    shortLink: String
    date: Date
}

const noteSchema: Schema = new Schema({
    note: {
        type: String,
        required: "Note is required",
        trim: false
    },
    shortLink: {
        type: String,
        required: "ShortLink is required",
        trim: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

const noteDb = mongoose.connection.useDb(cfg.QUICK_NOTES_DB_NAME).
               model<INote>('Note', noteSchema, "QuickNotes")

export default noteDb;