import * as dotenv from "dotenv"

dotenv.config();

export const QUICK_NOTES_DB_NAME = process.env.QUICK_NOTES_DB_NAME as string; 
export const QUICK_NOTES_DB_CONNECTION_STRING = process.env.QUICK_NOTES_DB as string;
