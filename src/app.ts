import express, {Application} from 'express';
import bodyParser from 'body-parser';
import { BaseController } from './controllers/BaseController';
import mongoose from 'mongoose';
import * as dotenv from "dotenv"
import * as cfg from './configurations/serverConfiguration';

class App {
    public app: Application
    public port: number

    constructor(controllers: BaseController[], port: number) {
        this.app = express()
        this.port = port

        this.initializeMiddlewares()
        this.initializeControllers(controllers)
        this.initializeMongoose()
    }

    private initializeMiddlewares() {
        this.app.use(bodyParser.json({limit: '50mb'}))
        this.app.use(bodyParser.urlencoded({limit: '50mb', extended: false }))
    }

    private initializeControllers(controllers: BaseController[]) {
        controllers.forEach(ctrl => {
            this.app.use('/api/', ctrl.router)
        })
    }

    private initializeMongoose() {
        dotenv.config();

        mongoose.set('useUnifiedTopology', true)
        mongoose.set('useNewUrlParser', true)
        mongoose.set('useFindAndModify', false)

        mongoose.connect(cfg.QUICK_NOTES_DB_CONNECTION_STRING, { useNewUrlParser: true }, 
        () => {
            console.log('Connected to DB')
        })
    }

    public listen() {
        this.app.listen(this.port, () => {
            console.log(`App listening on the port ${this.port}`)
        })
    }
}

export default App