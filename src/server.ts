import App from './app'
import { TestController } from './controllers/TestController'
import { NoteController } from './controllers/NoteController'

const app = new App (
    [
        new TestController('/test'),
        new NoteController('/note')
    ],
    Number(process.env.PORT) || 3001
)

app.listen()