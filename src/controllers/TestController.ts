import { BaseController } from "./BaseController";
import {Request, Response, NextFunction} from 'express';

export class TestController extends BaseController {

    constructor(path: string) {
        super(path)
        this.initializeRouters();
    }

    initializeRouters(): void {
        this.router.get(`${this.path}/hello`, this.getHello),
        this.router.post(`${this.path}/hello`, this.postHello)
    }

    async getHello(req: Request, res: Response, next: NextFunction) {
        try {

            res.status(200).send("I am alive!")

        } catch(error) {
            next(error)
        }
    }

    async postHello(req: Request, res: Response, next: NextFunction) {
        try {

            let body = req.body;

            res.status(200).send("I am alive!")

        } catch(error) {
            next(error)
        }
    }
}