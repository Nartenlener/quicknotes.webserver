import { BaseController } from "./BaseController";
import Note from "../models/noteModel"
import {Request, Response, NextFunction} from 'express';
import { INote } from '../models/noteModel';


export class NoteController extends BaseController {

    constructor(path: string) {
        super(path)
        this.initializeRouters()
    }

    initializeRouters(): void {
        this.router.post(`${this.path}/add`, this.addNote)
        this.router.get(`${this.path}/:shortLink`, this.getNote)
    }

    async getNote(req: Request, res: Response, next: NextFunction) {
        try {

            let noteFromDb = await Note.findOne({shortLink: req.params.shortLink})

            if(noteFromDb) {
                res.status(200).send(noteFromDb)
            }
            else {
                res.status(200).json({error: `Note id: "${req.params.shortLink}" does not exist`})
            }

        } catch (error) {
            res.status(500).send(error)
        }
    }
    
    async addNote(req: Request, res: Response, next: NextFunction) {
        try {

            let generatedShortLink: string = ""
            let noteExist = false

            do {
                
                await getShortLink().then(e => {
                    generatedShortLink = e
                });


                //check if short link exist
                let noteFromDb = await Note.findOne({shortLink: generatedShortLink})

                if(noteFromDb) {
                    noteExist = true
                }
            } while(noteExist)

            const note = new Note({
                note: req.body.note,
                shortLink: generatedShortLink
            })

            await note.save();

            res.status(200).json({shortLink: `${generatedShortLink}`})
            
        } catch (error) {
            res.status(500).send(error)
        }
    }
}

async function getShortLink(length: number = 5): Promise<string> {

    var result = ''
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    var charactersLength = characters.length
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }

    return result
}

